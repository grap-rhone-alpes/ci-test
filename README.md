# ci-test


# Installation

```
git clone https://gitlab.com/grap-rhone-alpes/ci-test/
cd ci-test
virtualenv env --python=python3
. ./env/bin/activate
poetry install
```

# Tests

1. by poetry

```
. env/bin/activate
poetry run pytest --cov ci_test -v
```

2. By tox

```
. env/bin/activate
tox
```

3. By gitlaci-local
```
. env/bin/activate
gitlabci-local
```


















python3 -m pip install --user pipx
python3 -m pipx ensurepath
su root
pipx install virtualenv
pipx install poetry
