from pathlib import Path

from click.testing import CliRunner
from loguru import logger

from ci_test.cli import main


def test_cli():

    docker_file = Path("./tests/data_files/Dockerfile")
    docker_file = docker_file

    result = CliRunner().invoke(
        main,
        [
            "--log-level=DEBUG",
            "cli-action",
        ],
        catch_exceptions=False,
    )
    if not result.exit_code == 0:
        logger.error("exit_code: %s" % result.exit_code)
        logger.error("output: %s" % result.output)
    assert result.exit_code == 0
