import docker
from loguru import logger


def get_docker_client():
    return docker.from_env()


def build_image(path, tag):
    if not path.is_dir() or not path.exists():
        raise Exception("Path folder %s not found.")

    if not (path / "Dockerfile").exists():
        raise Exception("Dockerfile %s not found.")

    logger.debug(
        "Building image named based on %s"
        " This can take a big while ..." % (path)
    )
    debug_docker_command = "docker build %s --tag %s" % (path, tag)
    logger.debug("DOCKER COMMAND:\n %s" % debug_docker_command)
    docker_client = get_docker_client()
    image = docker_client.images.build(
        path=str(path),
        tag=tag,
    )
    logger.debug("Image build.")
    return image
