import logging
import sys

import click
from click_loglevel import LogLevel
from loguru import logger

from ci_test.cli_action import cli_action


@click.group()
@click.option("-l", "--log-level", type=LogLevel(), default=logging.INFO)
@click.pass_context
def main(ctx, log_level):
    """
    Provides a command set to perform odoo Community Edition migrations.
    """
    logger.remove()
    logger.add(sys.stderr, level=log_level)
    logger.debug("Beginning script '%s' ..." % (ctx.invoked_subcommand))
    if not isinstance(ctx.obj, dict):
        ctx.obj = {}


main.add_command(cli_action)
